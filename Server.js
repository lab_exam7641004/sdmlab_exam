const express = require('express')
const cors = require('cors')

const app = express()
app.use(cors('*'))
app.use(express.json())

app.get('/', (request, response) => {
    response.send('node server created for jenkins')
})

app.listen(3000, '0.0.0.0', () => {
    console.log('server started on port 3000')
})